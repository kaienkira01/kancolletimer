#ifndef QT_UTIL_H
#define QT_UTIL_H

#include <memory>

#include <QAction>
#include <QObject>
#include <QMenu>
#include <QString>

namespace qtutil {

template <class Functor>
QAction *addActionToMenu(QMenu &menu, const QString &text, const Functor &trigger_cb)
{
    std::unique_ptr<QAction> action(new QAction(&menu));
    action->setText(text);
    QObject::connect(action.get(), &QAction::triggered, trigger_cb);
    QAction *action_raw = action.get();
    menu.addAction(action.release());

    return action_raw;
}

} // namespace qtutil

#endif