QT = gui widgets

TARGET = kancolletimer

TEMPLATE = app

SOURCES += main.cc \
    my_system_tray.cc

QMAKE_CXXFLAGS += -std=c++0x
QMAKE_LFLAGS += -static

RESOURCES += \
    kancolletimer.qrc

HEADERS += \
    my_system_tray.h \
    qt_util.h