#ifndef MY_SYSTEM_TRAY_H
#define MY_SYSTEM_TRAY_H

#include <QtGlobal>
#include <QList>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QWidget>
#include <QVector>

class MySystemTray : public QWidget {
public:
    MySystemTray(QWidget *parent = 0);
    virtual ~MySystemTray();

    bool init();

private:
    void onMenuAboutToShow();
    void onMenuAboutToHide();
    void onSetCustomTimerActionTriggered();
    void onSystemTrayActivated(QSystemTrayIcon::ActivationReason reason);
    void addTimer(int second);
    void removeTimer(int pos);
    void resortTimers();
    void onTimeout(QTimer *timer);
    void showLeftTime();
    void startNotify();
    void stopNotify();
    void notify();

private:
    QMenu menu_;
    QSystemTrayIcon systray_;
    QList<QTimer *> left_time_timers_;
    QVector<QAction *> left_time_hints_;
    QTimer countdown_timer_;
    QTimer notify_timer_;
};

#endif