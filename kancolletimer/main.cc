#include <QApplication>

#include "my_system_tray.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);

    MySystemTray systray;
    if (systray.init() == false) {
        return -1;
    }

    return app.exec();
}