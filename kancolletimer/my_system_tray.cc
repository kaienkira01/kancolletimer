#include "my_system_tray.h"

#include <functional>
#include <QtAlgorithms>
#include <QAction>
#include <QChar>
#include <QApplication>
#include <QInputDialog>
#include <QString>
#include <QStringList>

#include "qt_util.h"

#define MAX_TIMERS 3

MySystemTray::MySystemTray(QWidget *parent) :
    QWidget(parent), menu_(this), systray_(this),
    countdown_timer_(this), notify_timer_(this)
{
}

MySystemTray::~MySystemTray()
{
}

bool MySystemTray::init()
{
    for (int i = 0; i < MAX_TIMERS; ++i) {
        QAction *action = qtutil::addActionToMenu(
            menu_, tr("--:--:--"),
            std::bind(&MySystemTray::removeTimer, this, i));
        left_time_hints_.append(action);
    }
    menu_.addSeparator();
    qtutil::addActionToMenu(menu_, tr("自定义时间"),
        std::bind(&MySystemTray::onSetCustomTimerActionTriggered, this));
    qtutil::addActionToMenu(menu_, tr("远征2 00:30:00"),
        std::bind(&MySystemTray::addTimer, this, 29 * 60));
    qtutil::addActionToMenu(menu_, tr("远征3 00:20:00"),
        std::bind(&MySystemTray::addTimer, this, 19 * 60));
    qtutil::addActionToMenu(menu_, tr("远征5 01:30:00"),
        std::bind(&MySystemTray::addTimer, this, 89 * 60));
    qtutil::addActionToMenu(menu_, tr("远征6 00:40:00"),
        std::bind(&MySystemTray::addTimer, this, 39 * 60));
    qtutil::addActionToMenu(menu_, tr("远征21 02:20:00"),
        std::bind(&MySystemTray::addTimer, this, 139 * 60));
    qtutil::addActionToMenu(menu_, tr("远征37 02:45:00"),
        std::bind(&MySystemTray::addTimer, this, 164 * 60));
    qtutil::addActionToMenu(menu_, tr("远征38 02:55:00"),
        std::bind(&MySystemTray::addTimer, this, 174 * 60));
    menu_.addSeparator();
    menu_.addAction(tr("退出"), qApp, SLOT(quit()));

    connect(&menu_, &QMenu::aboutToShow,
            this, &MySystemTray::onMenuAboutToShow);
    connect(&menu_, &QMenu::aboutToHide,
            this, &MySystemTray::onMenuAboutToHide);

    systray_.setIcon(QIcon(":/res/tray_icon.ico"));
    systray_.setContextMenu(&menu_);
    systray_.show();

    connect(&countdown_timer_, &QTimer::timeout,
            this, &MySystemTray::showLeftTime);
    connect(&notify_timer_, &QTimer::timeout,
            this, &MySystemTray::notify);

    connect(&systray_, &QSystemTrayIcon::activated,
            this, &MySystemTray::onSystemTrayActivated);

    return true;
}

void MySystemTray::onMenuAboutToShow()
{
    showLeftTime();
    countdown_timer_.start(1000);
}

void MySystemTray::onMenuAboutToHide()
{
    countdown_timer_.stop();
}

void MySystemTray::onSetCustomTimerActionTriggered()
{
    QString time = QInputDialog::getText(this,
        tr(""), tr("输入时间(格式 HH:MM | HH:MM:SS)"));

    int h = 0;
    int m = 0;
    int s = 0;

    QStringList ret = time.split(":");
    if (ret.size() == 2) {
        h = ret[0].toInt();
        m = ret[1].toInt() % 60;
    } else if (ret.size() == 3) {
        h = ret[0].toInt();
        m = ret[1].toInt() % 60;
        s = ret[2].toInt() % 60;
    }

    addTimer(h * 3600 + m * 60 + s);
}

void MySystemTray::onSystemTrayActivated(
    QSystemTrayIcon::ActivationReason)
{
    stopNotify();
}

void MySystemTray::addTimer(int second)
{
    if (left_time_timers_.size() >= MAX_TIMERS) {
        return;
    }

    std::unique_ptr<QTimer> timer(new QTimer(this));
    connect(timer.get(), &QTimer::timeout,
            std::bind(&MySystemTray::onTimeout, this, timer.get()));
    timer->start(second * 1000);
    left_time_timers_.append(timer.release());

    resortTimers();
}

void MySystemTray::removeTimer(int pos)
{
    if (pos < 0 || pos >= left_time_timers_.size()) {
        return;
    }

    delete left_time_timers_[pos];
    left_time_timers_.removeAt(pos);
}

void MySystemTray::resortTimers()
{
    qSort(left_time_timers_.begin(), left_time_timers_.end(),
          [](const QTimer *lhs, const QTimer *rhs) {
              return lhs->remainingTime() < rhs->remainingTime();
          });
}

void MySystemTray::onTimeout(QTimer *timer)
{
    for (int i = 0; i < left_time_timers_.size(); ++i) {
        if (timer == left_time_timers_[i]) {
            left_time_timers_.removeAt(i);
            delete timer;
            break;
        }
    }
    startNotify();
}

void MySystemTray::showLeftTime()
{
    int i = 0;
    for (; i < left_time_timers_.size(); ++i) {
        int ms = left_time_timers_[i]->remainingTime();
        int h =  ms / (1000 * 60 * 60);
        int m = (ms - h * 1000 * 60 * 60) / (1000 * 60);
        int s = (ms - h * 1000 * 60 * 60 - m * 1000 * 60) / 1000;
        left_time_hints_[i]->setText(
            QString(tr("%1:%2:%3"))
                .arg(h, 2, 10, QChar('0'))
                .arg(m, 2, 10, QChar('0'))
                .arg(s, 2, 10, QChar('0')));
    }
    for (; i < MAX_TIMERS; ++i) {
        left_time_hints_[i]->setText(tr("--:--:--"));
    }
}

void MySystemTray::startNotify()
{
    if (notify_timer_.isActive()) {
        return;
    }
    notify();
    notify_timer_.start(1000);
}

void MySystemTray::stopNotify()
{
    if (notify_timer_.isActive()) {
        notify_timer_.stop();
    }
}

void MySystemTray::notify()
{
    systray_.showMessage(tr(""), tr("Done"));
}